// Définition de la fonction asynchrone appelée getUsers afin d'appeler l'API souhaitée
async function getUsers() {

    // Attente de la réponse de la requête fetch vers l'API JSONPlaceholder pour récupérer la liste des utilisateurs
    const response = await fetch("https://jsonplaceholder.typicode.com/users");
    // Attente de la transformation du fichier API en format JSON
    const UsersList = await response.json();

    // Boucle "For" permettant de boucler chaque utilisateur dans la liste
    for (const user of UsersList) {
        //Affichage dans la console du nom et adresse postale de chaque utilisateur
        console.log(user.name, user.address);
    }
}

//Appel de la fonction getUsers
getUsers();

